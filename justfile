run-database:
    docker compose --env-file ./.env.development up -d
clear-database:
    docker compose --env-file ./.env.development down
migrate:
    just run-database
    yarn dotenv -e .env.development yarn prisma migrate dev --name init
    echo "Done ! Let's see http://localhost:8080/"
reset-database: 
    just run-database
    yarn dotenv -e .env.development yarn prisma migrate reset
    echo "Done ! you've just break the db"
generate:
    yarn dotenv -e .env.development yarn prisma generate
run:
    just run-database
    yarn dev