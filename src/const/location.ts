export const LOCATIONS = {
    EN: 'en',
    RU: 'ru',
    DE: 'de',
    FR: 'fr',
    ES: 'es',
    IT: 'it',
    PT: 'pt',
    TR: 'tr',
    ZH: 'zh',
    JA: 'ja',
    KO: 'ko'
}