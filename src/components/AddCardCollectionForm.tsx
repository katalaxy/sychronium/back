import React from 'react'

export const AddCardCollectionForm = () => {
    return (
        <div className="max-w-sm mx-auto">
            <div className="mb-5">
                <label className="block mb-2 text-sm font-medium text-dark-900 dark:text-dark">Card ID</label>
                <input id="card-id-form" type="email" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="00000000" required />
            </div>
            <div className="mb-5">
                <label className="block mb-2 text-sm font-medium text-dark-900 dark:text-dark">Card set ID</label>
                <input id="card-set-id-form" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required placeholder='0000-XX00' />
            </div>
        </div>
    )
}