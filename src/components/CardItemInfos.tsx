import { Card, CardImage, UserCard, banlistInfo, CardPrice } from '@prisma/client'
import React from 'react'
import Image from 'next/image'
import { useIsLg } from '../hooks/useIsLg'


interface CardItemInfoProps {
    data: UserCard & { card: Card & { card_images: CardImage[], banlist_info?: banlistInfo, card_prices?: CardPrice[] } }
}

export const CardItemInfos = ({ data }: CardItemInfoProps) => {
    const goat = data.card.banlist_info?.ban_goat || 'Unlimited'
    const ocg = data.card.banlist_info?.ban_ocg || 'Unlimited'
    const tcg = data.card.banlist_info?.ban_tcg || 'Unlimited'

    const goatColor: string = goat === 'Unlimited' ? 'bg-green-500' : goat === 'Limited' ? 'bg-orange-500' : goat === 'Semi-Limited' ? 'bg-yellow-500' : 'bg-red-500'
    const ocgColor: string = ocg === 'Unlimited' ? 'bg-green-500' : ocg === 'Limited' ? 'bg-orange-500' : ocg === 'Semi-Limited' ? 'bg-yellow-500' : 'bg-red-500'
    const tcgColor: string = tcg === 'Unlimited' ? 'bg-green-500' : tcg === 'Limited' ? 'bg-orange-500' : tcg === 'Semi-Limited' ? 'bg-yellow-500' : 'bg-red-500'

    const isLg = useIsLg()

    return (
        <div className="flex lg:flex-row flex-col h-full w-full">
            <div className="flex-1 h-full w-full">
                <div className="flex f-full w-full flex-row justify-center align-middle">
                    <div className="max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
                        <Image width={0} height={0} sizes="100vw" style={{ width: '100%', height: 'auto', maxHeight: 382 }} className="rounded-t-lg" src={data.card.card_images[0]?.image_url_cropped} alt="" />
                        <div className="p-5">
                            <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">{data.card.name}</h5>
                            <div className="text-xs text-gray-400">
                                {data.card.type} · {data.card.race}
                            </div>
                            <div className="inline-flex rounded-md shadow-sm" role="group">
                                <span className={`px-4 py-2 text-sm font-medium text-white ${goatColor}  rounded-s-lg`}>
                                    <div className='font-extrabold'>GOAT</div>{goat}
                                </span>
                                <span className={`px-4 py-2 text-sm font-medium text-white ${ocgColor}`}>
                                    <div className='font-extrabold'>OCG</div>{ocg}
                                </span>
                                <span className={`px-4 py-2 text-sm font-medium text-white ${tcgColor}  rounded-e-lg`}>
                                    <div className='font-extrabold'>TCG</div>{tcg}
                                </span>
                            </div>
                            <div className='flex flex-row flex-wrap justify-center m-2'>
                                {Number(data.card?.card_prices?.[0]?.amazon_price) > 0 && <span className="bg-green-900 px-2 text-green-300 text-sm font-medium me-2 rounded m-1">💵 Amazon: ${data.card?.card_prices?.[0]?.amazon_price}</span>}
                                {Number(data.card?.card_prices?.[0]?.cardmarket_price) > 0 && <span className="bg-green-900 px-2 text-green-300 text-sm font-medium me-2 rounded m-1">💵 Card Market: ${data.card?.card_prices?.[0]?.cardmarket_price}</span>}
                                {Number(data.card?.card_prices?.[0]?.coolstuffinc_price) > 0 && <span className="bg-green-900 px-2 text-green-300 text-sm font-medium me-2 rounded m-1">💵 Cool Stuff.inc: ${data.card?.card_prices?.[0]?.coolstuffinc_price}</span>}
                                {Number(data.card?.card_prices?.[0]?.ebay_price) > 0 && <span className="bg-green-900 px-2 text-green-300 text-sm font-medium me-2 rounded m-1">💵 Ebay: ${data.card?.card_prices?.[0]?.ebay_price}</span>}
                                {Number(data.card?.card_prices?.[0]?.tcgplayer_price) > 0 && <span className="bg-green-900 px-2 text-green-300 text-sm font-medium me-2 rounded m-1">💵 Tcg Player: ${data.card.card_prices?.[0]?.tcgplayer_price}</span>}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="flex-1 h-full">
                <div className="flex flex-col h-full justify-center">
                    <div className='overflow-auto' style={{ maxHeight: isLg ? 680 : '100%' }}>

                        <div className='flex flex-row flex-wrap justify-center m-2'>
                            {data.card?.frameType && <span className="bg-blue-900 text-blue-300 text-sm font-medium me-2 rounded m-1 px-2">Frame type: {data.card?.frameType}</span>}
                            {data.card?.attribute && <span className="bg-blue-900 text-blue-300 text-sm font-medium me-2 rounded m-1 px-2">Attribute: {data.card?.attribute}</span>}
                            {data.card?.race && <span className="bg-blue-900 text-blue-300 text-sm font-medium me-2 rounded m-1 px-2">Race: {data.card?.race}</span>}
                            {data.card?.archetype && <span className="bg-blue-900 text-blue-300 text-sm font-medium me-2 rounded m-1 px-2">Archetype: {data.card?.archetype}</span>}
                            {data.card?.level && <span className="bg-blue-900 text-blue-300 text-sm font-medium me-2 rounded m-1 px-2">Level: {data.card?.level}</span>}
                            {data.card?.atk && <span className="bg-blue-900 text-blue-300 text-sm font-medium me-2 rounded m-1 px-2">ATK: {data.card?.atk}</span>}
                            {data.card?.def && <span className="bg-blue-900 text-blue-300 text-sm font-medium me-2 rounded m-1 px-2">DEF: {data.card?.def}</span>}
                            {data.card?.scale && <span className="bg-blue-900 text-blue-300 text-sm font-medium me-2 rounded m-1 px-2">Scale: {data.card?.scale}</span>}
                            {data.card?.linkval && <span className="bg-blue-900 text-blue-300 text-sm font-medium me-2 rounded m-1 px-2">Link value: {data.card?.linkval}</span>}
                            {Boolean(data.card?.linkmarkers?.length) && <span className="bg-blue-900 text-blue-300 text-sm font-medium me-2 rounded m-1 px-2">Link markers: {data.card?.linkmarkers.join(' ').replace("Bottom-Left", "↙️").replace("Middle-Left", "⬅️").replace("Top-Left", "↖️").replace("Bottom-Right", "↘️").replace("Middle-Right", "➡️").replace("Top-Right", "↗️").replace("Top", "⬆️").replace("Bottom", "⬇️")}</span>}
                        </div>
                        <div>
                            {
                                data.card.type.includes("Pendulum") ?
                                    <div>
                                        <h1 className="text-white font-bold m-3">
                                            Pendulum Effect
                                        </h1>
                                        <div className="text-sm text-gray-400 border-2 border-gray-600 h-1/2 overflow-auto p-1">
                                            {data.card.pend_desc}
                                        </div>
                                        <h1 className="text-white font-bold m-3">
                                            Monster Effect
                                        </h1>
                                        <div className="text-sm text-gray-400 border-2 border-gray-600 h-1/2 overflow-auto p-1">
                                            {data.card.monster_desc}
                                        </div>
                                    </div>
                                    :
                                    <div>
                                        <h1 className="text-white font-bold">
                                            Description
                                        </h1>
                                        <p className="text-sm text-gray-400 border-2 border-gray-600 h-3/4 overflow-auto p-1">
                                            {data.card.desc}
                                        </p>
                                    </div>
                            }
                        </div>
                        {
                            data.card?.atk &&
                            <div>
                                <h1 className="text-white font-bold m-3">
                                    ATK
                                </h1>
                                <div className='flex flex-row'>
                                    <div className='flex-1 text-gray-400'>
                                        {data.card.atk}
                                    </div>
                                    <div className="flex-1">
                                        10000
                                    </div>
                                </div>
                                <div className="w-full bg-gray-200 rounded-full h-2.5 dark:bg-gray-700">
                                    <div className="bg-blue-600 h-2.5 rounded-full" style={{ width: `${(data.card.atk * 100) / 10000}%` }}></div>
                                </div>
                            </div>
                        }
                        {
                            data.card?.def &&
                            <div>
                                <h1 className="text-white font-bold m-3">
                                    DEF
                                </h1>
                                <div className='flex flex-row'>
                                    <div className='flex-1 text-gray-400'>
                                        {data.card.def}
                                    </div>
                                    <div className="flex-1">
                                        10000
                                    </div>
                                </div>
                                <div className="w-full bg-gray-200 rounded-full h-2.5 dark:bg-gray-700">
                                    <div className="bg-blue-600 h-2.5 rounded-full" style={{ width: `${(data.card.def * 100) / 10000}%` }}></div>
                                </div>
                            </div>
                        }
                    </div>
                </div>
            </div>
        </div>

    )
}