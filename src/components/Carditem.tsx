import React from "react";
import Image from 'next/image'
import { Swal } from "../hocs/Swal";
import { CardItemInfos } from "./CardItemInfos";
import { UserCard, Card, CardImage } from "@prisma/client";
import { useIsLg } from "../hooks/useIsLg";

interface CardItemProps {
    data: UserCard & {card: Card & {card_images: CardImage[]}}
    ref?: React.Ref<any>
    deletable?: boolean
    onDelete?: () => Promise<void>
}


export const CardItem = ({data, ref, onDelete, deletable}: CardItemProps) => {
    const isLg = useIsLg()
    return (
        <a ref={ref} className="cursor-pointer m-3" onClick={() => !deletable && Swal.fire({title: <CardItemInfos data={data}/> , background: '#111827', width:  isLg ? '80%' : '100%'})}>
            {deletable && <button onClick={onDelete} className="focus:outline-none relative top-8 left-40 text-white bg-red-500 hover:bg-red-600 focus:ring-4 focus:ring-red-400 font-medium rounded-full text-sm px-5 py-2.5 me-2 mb-2 select-none">X</button>}
            <Image 
                src={data?.card?.card_images[0]?.image_url}
                width={200}
                height={200}
                alt={`${data?.card?.name} card`} 
            />
        </a>
    )
}