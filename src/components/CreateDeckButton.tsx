import React from 'react'
import { Swal } from '../hocs/Swal'
import { AddDeckForm } from './AddDeckForm'
import { createDeck } from '../api/createDeck'

export const CreateDeckButton = () => {
    return (
        <div className='flex m-5 flex-col h-96 w-52 rounded-lg bg-gray-800 border-8 border-gray-500 justify-center align-middle cursor-pointer' onClick={() => Swal.fire({
            title: <AddDeckForm/>,
            showCancelButton: true, 
            confirmButtonText: 'Create a new deck',
            preConfirm: async () => {
                try {
                    //@ts-ignore
                    const data = await createDeck(String(document.getElementById('deck-name-form')?.value))
                    if(/^\d+$/.test(String(data.data.data.id))) window.location.href = `/decks/${String(data.data.data.id)}`
                }catch (e) {
                    // TODO: add error toast
                }
            },
            allowOutsideClick: () => !Swal.isLoading() 
        })}>
            <div className='text-gray-500 text-9xl select-none text-center'>
                +
            </div>
        </div>
    )
}