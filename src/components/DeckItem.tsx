import { UserCard, Card, Deck, CardImage } from "@prisma/client"
import Image from 'next/image'
import Link from "next/link"


interface DeckItemDataType extends Deck {
    cards: Array<UserCard & { card: Card & {card_images: Array<CardImage>}}>
}


export const DeckItem = ({ onClick, data }: { onClick: () => void, data: DeckItemDataType }) => {
    return (
        <Link href={`/decks/${data.id}`} className="max-w-sm m-5 border rounded-lg shadow bg-gray-800 border-gray-700 cursor-pointer" onClick={onClick}>
            <Image width={0} height={0} sizes="100vw" style={{ width: '100%', height: 'auto', maxHeight: 382 }} className="rounded-t-lg" src={data?.cards?.[0]?.card?.card_images?.[0]?.image_url_cropped || '/static/assets/images/noCard.jpg'} alt="" />
            <div className="p-5 text-center">
                <h5 className="mb-2 text-2xl font-bold tracking-tight text-white">{data.name}</h5>
            </div>
        </Link>
    )
}