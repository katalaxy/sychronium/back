import { instance } from './instance'

export const sendMessage = async (email: string, subject: string, message: string) => {

    return await instance('/api/contact/post', {method:"POST", params:{email, subject, message}})
}