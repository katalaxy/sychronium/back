import { instance } from './instance'

export const addCardToDeck = async (deckId: string, userCardId: string) => {

    return await instance('/api/cards/deck/cards/post', {method:"POST", params:{deckId, userCardId}})
}