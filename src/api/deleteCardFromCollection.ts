import { instance } from './instance'

export const deleteCardFromCollection = async (userCardId: string) => {

    return await instance('/api/cards/collection/delete', {method:"DELETE", params:{userCardId}})
}