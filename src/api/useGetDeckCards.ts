import { instance } from "./instance";
import {useState, useEffect} from 'react'

export const useGetDeckCards = ({trigger, deckId}: {trigger?: number, deckId: string}) => {
    const [result, setResult] = useState<{data: any, isLoading: boolean, error: any}>({data: null, isLoading: true, error: null})

    useEffect(() => {
        instance(`/api/cards/deck/cards/get?deckId=${String(deckId)}`).then(data => {
            const res = data.data
            setResult({data: res.data, error: null, isLoading: false})
        }).catch(e => {
            setResult({data: null, error: e, isLoading: false})
        })
    }, [trigger])

    return result 
}