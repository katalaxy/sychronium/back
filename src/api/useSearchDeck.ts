import { instance } from "./instance";
import {useState, useEffect} from 'react'

export const useSearchDeck = ({search, trigger}: {search: string, trigger?: number}) => {
    const [result, setResult] = useState<{data: any, isLoading: boolean, error: any}>({data: null, isLoading: true, error: null})

    useEffect(() => {
        instance(`/api/cards/deck/search?term=${String(search)}`).then(data => {
            const res = data.data
            setResult({data: res.data, error: null, isLoading: false})
        }).catch(e => {
            setResult({data: null, error: e, isLoading: false})
        })
    }, [search, trigger])

    return result 
}