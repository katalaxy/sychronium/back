import { instance } from './instance'

export const createDeck = async (name: string) => {

    return await instance('/api/deck/post', {method:"POST", params:{name}})
}