import { instance } from './instance'

export const deleteDeck = async (deckId: string) => {

    return await instance('/api/deck/delete', {method:"DELETE", params:{deckId}})
}