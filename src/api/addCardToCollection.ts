import { instance } from './instance'

export const addCardToCollection = async (cardId: string, setId: string) => {

    return await instance('/api/cards/collection/post', {method:"POST", params:{setId, cardId}})
}