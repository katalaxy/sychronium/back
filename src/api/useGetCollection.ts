import { instance } from "./instance";
import {useState, useEffect} from 'react'

export const useGetCollection = (deps?: any) => {
    const [result, setResult] = useState<{data: any, isLoading: boolean, error: any}>({data: null, isLoading: true, error: null})

    useEffect(() => {
        instance('/api/cards/collection/get').then(data => {
            const res = data.data
            setResult({data: res.data, error: null, isLoading: false})
        }).catch(e => {
            setResult({data: null, error: e, isLoading: false})
        })
    }, [deps])

    return result 
}