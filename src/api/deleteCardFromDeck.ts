import { instance } from './instance'

export const deleteCardFromDeck = async (deckId: string, userCardId: string) => {

    return await instance('/api/cards/deck/cards/delete', {method:"DELETE", params:{deckId, userCardId}})
}