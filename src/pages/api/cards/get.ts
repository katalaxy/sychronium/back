import { NextApiRequest, NextApiResponse } from "next";
import { userOr403 } from "../../../utils/userOr403";
import prisma from '../../../lib/prisma'
import { imageCacher } from "../../../utils/imageCacher";

export default async (req: NextApiRequest, res: NextApiResponse<any>) => {
    const user = await userOr403(req, res)
    if (!user) return;
    
    let result = await prisma.card.findMany({
        include: {
            
            card_sets: true,
            card_images: true,

        },
        where: {
            id: Number(req.query.id),
        }
    })

    result = await Promise.all(result.map(async card => {
        if(card.is_images_cached) {
            return card;
        }
        else {
            const cachedImagesUrls = await imageCacher(card.id)
            if(cachedImagesUrls) {
                return {...card, card_images: [...cachedImagesUrls]}
            }
            else {
                return {...card, card_images: []}
            }
        }
    }))

    let resultWithSetCodeFilter;
    if(req.query.set_id){
        resultWithSetCodeFilter = result.map(card => ({...card, card_sets: card.card_sets.filter(set => set.set_code.indexOf(String(req.query.set_id).split('-')[0]) !== -1)}))
        if(!resultWithSetCodeFilter || resultWithSetCodeFilter.length === 0) resultWithSetCodeFilter = undefined;
    }
    res.status(200).json({data: resultWithSetCodeFilter || result})
}