import { NextApiRequest, NextApiResponse } from "next";
import { userOr403 } from "../../../../utils/userOr403";
import prisma from "../../../../lib/prisma";

export default async (req: NextApiRequest, res: NextApiResponse<any>) => {
    const user = await userOr403(req, res)
    if (!user) return;

    const { term } = req.query

    const decks = await prisma.deck.findMany({
        include: {
            cards: {
                include: {
                    card: {
                        include: {
                            banlist_info: true, 
                            card_images: true,
                            card_prices: true,
                            card_sets: true,
                        }
                    }
                }
            }
        },
        where:{
            userId: user.id
        }
    })

    res.status(200).json({data: decks.filter(deck => JSON.stringify(deck).toLowerCase().includes(String(term).toLowerCase()))})
}