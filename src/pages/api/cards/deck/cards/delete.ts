import { NextApiRequest, NextApiResponse } from "next";
import { userOr403 } from "../../../../../utils/userOr403";
import prisma from '../../../../../lib/prisma'

export default async (req: NextApiRequest, res: NextApiResponse<any>) => {
    const user = await userOr403(req, res)
    if (!user) return;

    const { deckId, userCardId } = req.query

    const result = await prisma.deck.update({
        where: {
            id: Number(deckId),
            userId: user.id
        },
        data: {
            cards: {
                disconnect: {
                    id: Number(userCardId)
                }
            }
        }
    }).catch(e => {
        res.status(422).json({ message: 'Error updating deck', data: {...e, message: e.message} })
        return
    })
    
    res.status(200).json({ message: 'card removed from deck', data: result})
}