import { NextApiRequest, NextApiResponse } from "next";
import { userOr403 } from "../../../../../utils/userOr403";
import prisma from '../../../../../lib/prisma'

export default async (req: NextApiRequest, res: NextApiResponse<any>) => {
    const user = await userOr403(req, res)
    if (!user) return;

    const { deckId } = req.query

    const result = await prisma.deck.findUnique({
        include: {
            cards: {
                include: {
                    card: {
                        include: {
                            banlist_info: true,
                            card_images: true, 
                            card_prices: true,
                            card_sets: true,
                        }
                    }
                }
            },
        },
        where: {
            id: Number(deckId),
            userId: user.id
        }
    }).catch(e => {
        res.status(404).json({ message: 'Deck not found', data: {...e, message: e.message} })
        return
    })

    if(!result) res.status(404).json({ message: 'Deck not found'})
    
    res.status(200).json({data: result?.cards})
}