import { NextApiRequest, NextApiResponse } from "next";
import { userOr403 } from "../../../../utils/userOr403";
import prisma from '../../../../lib/prisma'
import { imageCacher } from "../../../../utils/imageCacher";
import { LOCATIONS } from "../../../../const/location";

export default async (req: NextApiRequest, res: NextApiResponse<any>) => {
    const user = await userOr403(req, res)
    if (!user) return;

    const { cardId, setId } = req.query

    const cardLocation = Object.values(LOCATIONS).find(location => String(setId).toLowerCase().includes(location))


    const userCollection = await prisma.collection.findUnique({
        include: {
            cards: true
        },
        where: {
            userId: user?.id
        }
    })

    const cards = await prisma.card.findMany({
        include: {
            card_sets: true,
        },
        where: {
            id: Number(cardId)
        }
    })

    let card;
    if(cardLocation){
        card = cards.find(card => card.card_sets.map(set => set.set_code).includes(String(setId).replace(cardLocation.toUpperCase(), 'EN')))
        if(!card) card = cards.find(card => card.language.toLowerCase() === cardLocation.toLowerCase())
    }
    else {
        card = cards[0]
    }

    if(!card) {
        res.status(404).json({ message: 'card not found'}) 
        return
    }

    if(!card?.is_images_cached) await imageCacher(card.id)

    if (userCollection) {
        const result = await prisma.userCard.create({
            data: {
                card_index: card.card_index,
                collectionId: userCollection.id,
                userId: user.id,
                set_code: String(setId),
            }
        }).catch(e => {
            res.status(422).json({ message: 'Error updating collection', data: {...e, message: e.message} })
            return
        })
        res.status(200).json({ message: 'card created', data: result})
    }
    else {
        const result = await prisma.collection.create({
            data: {
                userId: user.id,
                cards: {
                    create: [
                        {
                            card_index: card.card_index,
                            userId: user.id,
                            set_code: String(setId),
                        }
                    ]
                }
            }
        }).catch(e => {
            res.status(422).json({ message: 'Error creating collection', data: { ...e, message: e.message } })
            return
        })
        res.status(200).json({ message: 'Collection created & card added', result})
    }
}