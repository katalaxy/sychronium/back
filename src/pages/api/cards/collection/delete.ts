import { NextApiRequest, NextApiResponse } from "next";
import { userOr403 } from "../../../../utils/userOr403";
import prisma from '../../../../lib/prisma'

export default async (req: NextApiRequest, res: NextApiResponse<any>) => {
    const user = await userOr403(req, res)
    if (!user) return;

    const { userCardId } = req.query

    const result = await prisma.collection.update({
        where: {
            userId: user.id
        },
        data: {
            cards: {
                delete: {
                    id: Number(userCardId)
                }
            }
        }
    }).catch(e => {
        res.status(422).json({ message: 'Collection update error', data: {...e, message: e.message} })
        return
    })
    
    res.status(200).json({message: 'card deleted from collection', data: result})
}