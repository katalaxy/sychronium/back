import { NextApiRequest, NextApiResponse } from "next";
import { userOr403 } from "../../../utils/userOr403";
import prisma from '../../../lib/prisma'

export default async (req: NextApiRequest, res: NextApiResponse<any>) => {
    const user = await userOr403(req, res)
    if (!user) return;

    const { name } = req.query

    const result = await prisma.deck.create({
        data: {
            name: String(name),
            userId: user.id,
        }
    }).catch(e => {
        res.status(422).json({ message: 'Error creating deck', data: e })
        return
    })
    
    res.status(200).json({ message: 'Collection deck created' , data: result})
}