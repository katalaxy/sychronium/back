import { NextApiRequest, NextApiResponse } from "next";
import { userOr403 } from "../../../utils/userOr403";
import prisma from '../../../lib/prisma'

export default async (req: NextApiRequest, res: NextApiResponse<any>) => {
    const user = await userOr403(req, res)
    if (!user) return;

    const { deckId } = req.query

    const result = await prisma.deck.delete({
        where: {
            id: Number(deckId)
        }
    }).catch(e => {
        res.status(422).json({ message: 'Error deleting deck', data: e })
        return
    })
    
    res.status(200).json({ message: 'Collection deck deleting' , data: result})
}