import { NextApiRequest, NextApiResponse } from 'next';
import nodemailer from 'nodemailer';
import { loadEmailTemplate } from '../../../utils/loadEmailTemplate';

export default async (req: NextApiRequest, res: NextApiResponse<any>) => {
    if (req.method !== 'POST') {
        return res.status(405).end(); // Méthode non autorisée
    }

    const { email, subject, message } = req.query;

    // Vérifiez si les données requises sont présentes
    if (!email || !subject || !message) {
        return res.status(400).json({ error: 'Lack of information to perform your request' });
    }

    // Configurer le transporteur SMTP avec les variables d'environnement
    const transporter = nodemailer.createTransport({
        host: process.env.SMTP_HOST,
        port: 587,
        secure: false, // true pour le port 465, false pour les autres ports
        auth: {
            user: process.env.SMTP_USER,
            pass: process.env.SMTP_PASS
        }
    });

    // Envoyer un e-mail au support
    try {
        await transporter.sendMail({
            from: process.env.EMAIL_FROM_CONTACT,
            to: process.env.EMAIL_TO_CONTACT,
            subject: `Contact request from ${email} - ${subject}`,
            text: String(message)
        });

        // Envoyer une confirmation à l'expéditeur
        await transporter.sendMail({
            from: process.env.EMAIL_FROM,
            to: String(email),
            subject: 'Confirmation of receipt of your message - Syncronium',
            text: loadEmailTemplate('confirm-recived-message.html')
        });

        res.status(200).json({ message: 'E-mail succefully sended' });
    } catch (error) {
        console.warn(error)
        res.status(500).json({ error: 'Error throwed while sending email.' });
    }
}
