export default {
    "id": 21,
    "userId": "clubezkfu0000flf9qoay0s6x",
    "card_index": 19252,
    "set_code": "PHNI-FR098",
    "collectionId": 1,
    "card": {
        "card_index": 19252,
        "id": 97534104,
        "name": "Mystic Potato",
        "name_en": null,
        "type": "Effect Monster",
        "frameType": "effect",
        "desc": "If this card on the field is destroyed by card effect and sent to the GY: You can Special Summon 1 DARK monster with 1500 or less ATK from your Deck in Attack Position, except \"Mystic Potato\".",
        "race": "Plant",
        "atk": 1400,
        "def": 1100,
        "level": 4,
        "attribute": "DARK",
        "linkval": null,
        "linkmarkers": [],
        "archetype": "Attribute Summoner",
        "pend_desc": null,
        "monster_desc": null,
        "scale": null,
        "ygoprodeck_url": "https://ygoprodeck.com/card/mystic-potato-13861",
        "is_images_cached": true,
        "language": "EN",
        "banlist_info": null,
        "card_images": [
            {
                "image_index": 19511,
                "id": 97534104,
                "image_url": "/static/cdn/97534104/19511/normal.jpg",
                "image_url_small": "/static/cdn/97534104/19511/small.jpg",
                "image_url_cropped": "/static/cdn/97534104/19511/cropped.jpg",
                "cardId": 19252
            }
        ],
        "card_prices": [
            {
                "id": 19252,
                "cardmarket_price": "0.06",
                "tcgplayer_price": "0.09",
                "ebay_price": "7.99",
                "amazon_price": "0.00",
                "coolstuffinc_price": "0.00",
                "cardId": 19252
            }
        ],
        "card_sets": [
            {
                "id": 68950,
                "set_name": "Phantom Nightmare",
                "set_code": "PHNI-EN098",
                "set_rarity": "Common",
                "set_rarity_code": "(C)",
                "set_price": "0",
                "cardId": 19252
            }
        ]
    }
}