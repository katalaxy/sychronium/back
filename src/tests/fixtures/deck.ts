export default {
    "id": 1,
    "userId": "clubezkfu0000flf9qoay0s6x",
    "name": "tommate",
    "cards": [
        {
            "id": 2,
            "userId": "clubezkfu0000flf9qoay0s6x",
            "card_index": 42,
            "set_code": "LED6-EN028",
            "collectionId": 1,
            "card": {
                "card_index": 42,
                "id": 37675907,
                "name": "Accel Synchronique",
                "name_en": "Accel Synchron",
                "type": "Synchro Tuner Monster",
                "frameType": "synchro",
                "desc": "1 Syntoniseur + 1+ monstre non-Syntoniseur\nUne fois par tour : vous pouvez envoyer 1 monstre \"Synchronique\" depuis votre Deck au Cimetière, puis activez 1 de ces effets ;\n●Augmentez le Niveau de cette carte du Niveau du monstre envoyé.\n●Réduisez le Niveau de cette carte du Niveau du monstre envoyé.\nDurant la Main Phase de votre adversaire, vous pouvez (Effet Rapide) : immédiatement après la résolution de cet effet, Invoquez par Synchronisation en utilisant cette carte que vous contrôlez. Vous ne pouvez Invoquer par Synchronisation \"Accel Synchronique\" qu'une fois par tour.",
                "race": "Machine",
                "atk": 500,
                "def": 2100,
                "level": 5,
                "attribute": "DARK",
                "linkval": null,
                "linkmarkers": [],
                "archetype": "Synchron",
                "pend_desc": null,
                "monster_desc": null,
                "scale": null,
                "ygoprodeck_url": "https://ygoprodeck.com/card/accel-synchron-3187",
                "is_images_cached": true,
                "language": "FR",
                "banlist_info": null,
                "card_images": [
                    {
                        "image_index": 42,
                        "id": 37675907,
                        "image_url": "/static/cdn/37675907/11923/normal.jpg",
                        "image_url_small": "/static/cdn/37675907/11923/small.jpg",
                        "image_url_cropped": "/static/cdn/37675907/11923/cropped.jpg",
                        "cardId": 42
                    }
                ],
                "card_prices": [
                    {
                        "id": 42,
                        "cardmarket_price": "0.13",
                        "tcgplayer_price": "0.21",
                        "ebay_price": "0.99",
                        "amazon_price": "2.54",
                        "coolstuffinc_price": "0.39",
                        "cardId": 42
                    }
                ],
                "card_sets": [
                    {
                        "id": 147,
                        "set_name": "Legendary Duelists: Magical Hero",
                        "set_code": "LED6-EN028",
                        "set_rarity": "Common",
                        "set_rarity_code": "(C)",
                        "set_price": "0",
                        "cardId": 42
                    },
                    {
                        "id": 148,
                        "set_name": "Legendary Duelists: Season 3",
                        "set_code": "LDS3-EN120",
                        "set_rarity": "C|print=Reprint",
                        "set_rarity_code": "",
                        "set_price": "0",
                        "cardId": 42
                    },
                    {
                        "id": 149,
                        "set_name": "Legendary Duelists: Season 3",
                        "set_code": "LDS3-EN120",
                        "set_rarity": "Common",
                        "set_rarity_code": "(C)",
                        "set_price": "0.82",
                        "cardId": 42
                    },
                    {
                        "id": 150,
                        "set_name": "Synchron Extreme Structure Deck",
                        "set_code": "SDSE-EN042",
                        "set_rarity": "Super Rare",
                        "set_rarity_code": "(SR)",
                        "set_price": "0",
                        "cardId": 42
                    }
                ]
            }
        }
    ]
}