import React from 'react'
import { useSearchCollection } from '../api/useSearchCollection'
import { CardItem } from '../components/Carditem'
import { Draggable } from 'react-beautiful-dnd'


export const DraggableCardCollectionResultModule = ({search, trigger, ref}: {search?: string, trigger?: number, ref?: React.Ref<any>}) => {
    const {data, isLoading, error} = useSearchCollection({search: search || '', trigger})
    return (
        <div ref={ref} className='flex flex-row flex-wrap justify-center'>
            {data?.map((item: any, index: number) => {
                return (
                    <Draggable draggableId={`COLLECTION-${String(item?.id)}`} index={index} key={`COLLECTION-${String(item?.id)}`}>
                        {
                            (provided, snapshot) => (
                                <div ref={provided.innerRef} {...provided.draggableProps}>
                                    <div {...provided.dragHandleProps} className='mx-5'>
                                        <CardItem key={index} data={item}/>
                                    </div>
                                </div>
                            )
                        }
                    </Draggable>
                )
            })}
        </div>
    )
}