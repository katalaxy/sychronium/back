'use client'

import React from 'react'
import { CardItem } from '../components/Carditem'
import { useSearchCollection } from '../api/useSearchCollection'
import { deleteCardFromCollection } from '../api/deleteCardFromCollection'
import { Card, CardImage, UserCard } from '@prisma/client'
import { useIsLg } from '../hooks/useIsLg'

export const CardCollectionResultModule = ({search, trigger, setTrigger}: {search?: string, trigger?: number, setTrigger: React.Dispatch<React.SetStateAction<number>>}) => {
    const {data, isLoading, error} = useSearchCollection({search: search || '', trigger})
    const [deletable, setDeletable] = React.useState(false)
    const isLg = useIsLg()
    return (
        <div className='flex flex-row flex-wrap justify-center'>
            {data?.length && <button onClick={() => setDeletable(!deletable)} type="button" style={{left: isLg ? '85vw' : '75vw'}} className="fixed z-30 top-3/4 rounded-full text-white bg-gray-800 hover:gray-500 focus:ring-4 focus:ring-gray-700 font-medium text-sm px-5 py-2.5 me-2 mb-2 focus:outline-none"><div className='text-4xl'>{!deletable ? `🖊️ ${isLg ? 'edit' : ''}` : `👌 ${isLg ? 'ok' : ''}`}</div></button>}
            {data?.map((item: UserCard & {card: Card & {card_images: CardImage[]}}, index: number) => {
                return (
                    <CardItem key={index} data={item} onDelete={async () => {
                        try {
                            await deleteCardFromCollection(String(item.id))
                            setTrigger(Math.random())
                        } catch(e) {
                            //TODO: add error toast
                        }
                    }} deletable={deletable}/>
                )
            })}
        </div>
    )
}