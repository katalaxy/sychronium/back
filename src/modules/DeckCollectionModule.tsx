'use client'

import React from 'react'
import { useGetDeck } from '../api/useGetDeck'
import { DeckItem } from '../components/DeckItem'
import { useSearchDeck } from '../api/useSearchDeck'
import { CreateDeckButton } from '../components/CreateDeckButton'

export const DeckCollectionResultModule = ({search, trigger}: {search: string, trigger?: number}) => {
    const {data, isLoading, error} = useSearchDeck({trigger, search})
    return (
        <div className='flex flex-row flex-wrap justify-center'>
            <div className='flex flex-col justify-center'>
                <CreateDeckButton/>
            </div>
            {data?.map((item: any, index: number) => {
                return (
                    <DeckItem data={item} onClick={()=>{}} key={index}/>
                )
            })}
        </div>
    )
}