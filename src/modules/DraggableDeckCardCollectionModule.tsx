import React from 'react'
import { Draggable } from 'react-beautiful-dnd'
import { CardItem } from '../components/Carditem'
import { useGetDeckCards } from '../api/useGetDeckCards'
import { estimateCollectionPrice } from '../utils/estimateCollectionPrice'
import { Card, UserCard } from '@prisma/client'
import { getCollectionPolicyInformation } from '../utils/getCollectionPolicyInformation'
import { Swal } from '../hocs/Swal'
import { deleteDeck } from '../api/deleteDeck'

export const DraggableDeckCardCollectionModule = ({ trigger, ref, deckId }: { search?: string, trigger?: number, ref?: React.Ref<any>, deckId: string }) => {
    const { data, isLoading, error } = useGetDeckCards({ trigger, deckId })
    //const banCollectionInfo = getCollectionPolicyInformation(data?.map((userCards: UserCard & {card: Card}) => userCards.card))
    return (
        <div className='flex flex-col'>
            <div className="flex flex-row w-1/2 px-6 text-white justify-around py-9 h-10 fixed bg-gray-900">
                <div className="flex flex-row">
                    Count:
                    <div className={`font-bold ml-1 ${data?.length >= 40 && data?.length <= 60 ? 'text-white' : 'text-red-400'}`}>{data?.length || 0}</div>
                </div>
                <div className="flex flex-row">
                    Deck Price estimation:
                    <div className={`font-bold ml-1 text-green-300`}>{estimateCollectionPrice(data?.map((userCard: UserCard & {card: Card}) => userCard.card))}$</div>
                </div>
                <div className="flex flex-col relative bottom-2">
                    <button type="button" onClick={() => {
                        Swal.fire({title: 'Delete this deck ?', showCancelButton: true, confirmButtonText: 'Yes, delete it !', confirmButtonColor: '#ef4444', preConfirm: async () => {
                            try {
                                await deleteDeck(deckId)
                                window.location.href = '/decks'
                            } catch (e){
                                //TODO: add error toast
                            }
                        }})
                    }} className="focus:outline-none text-white bg-red-500 hover:bg-red-600 focus:ring-4 focus:ring-red-400 font-medium rounded-lg text-sm px-5 py-2.5 me-2 mb-2">Delete this deck</button>
                </div>
            </div>
            <div ref={ref} className='flex flex-row flex-wrap justify-center mt-24'>
                {data?.map((item: any, index: number) => {
                    return (

                        <Draggable draggableId={`DECK-${String(item?.id)}`} index={index} key={`DECK-${String(item?.id)}`}>
                            {
                                (provided, snapshot) => (
                                    <div ref={provided.innerRef} {...provided.draggableProps}>
                                        <div {...provided.dragHandleProps} className='mx-5'>
                                            <CardItem key={index} data={item} />
                                        </div>
                                    </div>
                                )
                            }
                        </Draggable>
                    )
                })}
            </div>
        </div>
    )
}