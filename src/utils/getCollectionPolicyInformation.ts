import { Card, banlistInfo} from ".prisma/client";

interface CardCollection  extends Card {
    banlist_info: banlistInfo | null
}

export const getCollectionPolicyInformation = (cards: Array<CardCollection>) => {
    let result = {
        tcg: {semiLimited: 0, limited: 0, ban: 0}, 
        ocg : {semiLimited: 0, limited: 0, ban: 0},
        goat: {semiLimited: 0, limited: 0, ban: 0}
    }

    cards?.forEach((card) => {
        switch(card?.banlist_info?.ban_goat){
            case 'Banned' :
                result.goat.ban+=1
            case 'Semi-Limited':
                result.goat.semiLimited+=1
            case 'Limited' :
                result.goat.limited+=1
        }

        switch(card?.banlist_info?.ban_ocg){
            case 'Banned' :
                result.ocg.ban+=1
            case 'Semi-Limited':
                result.ocg.semiLimited+=1
            case 'Limited' :
                result.ocg.limited+=1
        }

        switch(card?.banlist_info?.ban_tcg){
            case 'Banned' :
                result.tcg.ban+=1
            case 'Semi-Limited':
                result.tcg.semiLimited+=1
            case 'Limited' :
                result.tcg.limited+=1
        }
    })

    return result
}