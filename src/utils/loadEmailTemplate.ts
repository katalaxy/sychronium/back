import fs from 'fs';

export const loadEmailTemplate = (template: string) => {
    return fs.readFileSync(`${__dirname}/../../../../../public/static/templates/email/${template}`)
}