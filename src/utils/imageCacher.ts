import prisma from "../lib/prisma"
import Downloader from "nodejs-file-downloader";
import fs from 'fs'
import { CardImage } from "@prisma/client";
import path from 'path'

const imageUpdater = async (cardImage: CardImage, newImageUrls: { image_url: string, image_url_small: string, image_url_cropped: string }) => {
    await prisma.cardImage.updateMany({
        where: {
            id: cardImage.id
        },
        data: {
            image_url: newImageUrls.image_url,
            image_url_small: newImageUrls.image_url_small,
            image_url_cropped: newImageUrls.image_url_cropped
        }
    })

    await prisma.card.update({
        where: {
            card_index: cardImage.cardId
        },
        data: {
            is_images_cached: true
        }
    })

}

const imagesDownloader = async (cardImage: CardImage, imageStorageSpecificPath: string, imageApiRoutes: {image_url: string, image_url_small: string, image_url_cropped: string}) => {
    const isImagesExist = fs.existsSync(`${imageStorageSpecificPath}/normal.jpg`) && fs.existsSync(`${imageStorageSpecificPath}/cropped.jpg`) && fs.existsSync(`${imageStorageSpecificPath}/small.jpg`)
    if(isImagesExist) {
        return { ...cardImage, ...imageApiRoutes}
    }
    const normal = new Downloader({
        url: cardImage.image_url,
        directory: imageStorageSpecificPath,
        fileName: 'normal.jpg'
    });
    const small = new Downloader({
        url: cardImage.image_url_small,
        directory: imageStorageSpecificPath,
        fileName: 'small.jpg'
    })
    const cropped = new Downloader({
        url: cardImage.image_url_cropped,
        directory: imageStorageSpecificPath,
        fileName: 'cropped.jpg'
    })

    try {
        if(!isImagesExist){
            await normal.download();
            await small.download();
            await cropped.download();
        }
        const imageUrlsUpdated = { ...cardImage, ...imageApiRoutes}
        await imageUpdater(cardImage, imageUrlsUpdated)
        return imageUrlsUpdated;
    } catch (error) {
        console.warn(error)
        return cardImage
    }
}

const imageshandler = async (cardImages: CardImage[]) => {
    await Promise.all(cardImages.map(async (image) => {
        const publicPath = path.join(process.cwd(), 'public');

        const imageApiRoutes = {
            image_url: `/static/cdn/${image.id}/${image.image_index}/normal.jpg`,
            image_url_small: `/static/cdn/${image.id}/${image.image_index}/small.jpg`,
            image_url_cropped: `/static/cdn/${image.id}/${image.image_index}/cropped.jpg`
        }

        const imageStorageSpecificPath = `${publicPath}/static/cdn/${image.id}/${image.image_index}`

        const isImagesExist = fs.existsSync(`${imageStorageSpecificPath}/normal.jpg`) && fs.existsSync(`${imageStorageSpecificPath}/cropped.jpg`) && fs.existsSync(`${imageStorageSpecificPath}/small.jpg`)

        if (fs.existsSync(imageStorageSpecificPath)) {
            if(isImagesExist){
                return {
                    ...image,
                    ...imageApiRoutes
                }
            }
            else {
                fs.rmSync(imageStorageSpecificPath, { recursive: true })
            }
        }
        else {
            fs.mkdirSync(imageStorageSpecificPath, { recursive: true });
        }

        return await imagesDownloader(image, imageStorageSpecificPath, imageApiRoutes)


    }))
}

export const imageCacher = async (id: number) => {

    const cardImages = await prisma.cardImage.findMany({
        where: {
            id: id
        }
    })

    if (!cardImages) return [];

    return await imageshandler(cardImages)
}