import { Card, CardPrice } from ".prisma/client";

interface CardCollection  extends Card {
    card_prices: Array<CardPrice>
}

export const estimateCollectionPrice = (cards: Array<CardCollection>) => {
    let totalPrice = 0;

    console.log(cards)

    cards?.forEach((card) => {
        let cardPriceTotal = 0;
        let priceCount = 0;
        
        card?.card_prices?.forEach((prices) => {
            //@ts-ignore
            delete prices.id
            //@ts-ignore
            delete prices.cardId
            Object.values(prices)?.forEach((price) => {
                if(Number(price) > 0){
                    cardPriceTotal += Number(price)
                    priceCount += 1
                } 
            })
        })
        totalPrice += cardPriceTotal / priceCount

    })

    return Math.round(totalPrice * 100)/100
    
}