import { NextApiResponse,  NextApiRequest} from "next";
import prisma from "../lib/prisma";

export const userOr403 = async (req: NextApiRequest, res: NextApiResponse<any>) => {
    const sessionToken = req.cookies["next-auth.session-token"]
    if (!sessionToken) {
        res.status(403).json({ message: "Unauthorized" })
        return null
    }
    const userSession = await prisma.session.findFirst({
        where: {
            sessionToken: sessionToken
        }
    })
    if (!userSession) {
        res.status(403).json({ message: "Unauthorized" })
        return null
    }
    const user = await prisma.user.findFirst({
        where: {
            id: userSession.userId
        }
    })
    return user
}