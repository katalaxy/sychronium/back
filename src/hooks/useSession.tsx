'use client'

import { Session } from "next-auth"
import { getSession } from "next-auth/react"
import React from "react"

export const useSession = (): (Session & {
    sessionToken?: string | undefined;
}) | null => {
    const [session, setSession] = React.useState<Session & {sessionToken?: string}| null>(null);
    React.useEffect(() => {
        getSession().then(setSession);
    }, [])
    return session
}