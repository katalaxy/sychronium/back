'use client'

import React from "react";
import { DragDropContext, Droppable } from 'react-beautiful-dnd';
import { DashboardSearchBar } from "../../../components/DashboardSearchBar";
import { Navbar } from "../../../components/Navbar";
import { DraggableCardCollectionResultModule } from "../../../modules/DraggableCardCollectionResultModule";
import { DraggableDeckCardCollectionModule } from "../../../modules/DraggableDeckCardCollectionModule";
import { addCardToDeck } from "../../../api/addCardToDeck";
import { deleteCardFromDeck } from "../../../api/deleteCardFromDeck";

export default function builder() {
    const [search, setSearch] = React.useState('')
    const [deckCollectionTrigger, setDeckCollectoionTrigger] = React.useState(0)
    const [deckId, setDeckId] = React.useState(NaN)

    React.useEffect(() => {
        if(/^\d+$/.test(String(window.location.pathname.split("/").pop()))){
            setDeckId(Number(window.location.pathname.split("/").pop()))
        }
    }, [window.location.pathname])

    return (
        <div className="flex flex-col w-full h-screen">
            <div className="fixed w-full bg-gray-900">
                <Navbar />
            </div>
            <div className="flex flex-row mt-32 h-full">
                <DragDropContext onDragEnd={async (result) => { 
                    if(result.destination?.droppableId === 'deck') {
                        try {
                            const deckId = String(window.location.pathname.split("/").pop())
                            const UserCardId = result.draggableId.replace('COLLECTION-', '')
                            await addCardToDeck(deckId, UserCardId)
                            setDeckCollectoionTrigger(Math.random())
                        } catch(e) {
                            //TODO: add error toast
                        }
                    }
                    if(result.destination?.droppableId === 'collection') {
                        try {
                            const deckId = String(window.location.pathname.split("/").pop())
                            const UserCardId = result.draggableId.replace('DECK-', '')
                            console.log(deckId, UserCardId)
                            await deleteCardFromDeck(deckId, UserCardId)
                            setDeckCollectoionTrigger(Math.random())
                        } catch(e) {
                            //TODO: add error toast
                        }
                    }
                }}>
                    <div className="flex-1">
                        <Droppable droppableId="deck">
                            {
                                (provided) => (
                                    <div ref={provided.innerRef} className="flex flex-row overflow-auto" style={{ height: '86vh' }}>
                                        {/^\d+$/.test(String(deckId)) && <DraggableDeckCardCollectionModule deckId={String(deckId)} trigger={deckCollectionTrigger}/>}
                                    </div>
                                )
                            }
                        </Droppable>
                    </div>
                    <div className="border border-gray-700" />
                    <div className="flex-1">
                        <DashboardSearchBar setSearch={setSearch} />
                        <Droppable droppableId="collection">
                            {
                                (provided) => (
                                    <div ref={provided.innerRef} className="flex flex-row overflow-auto" style={{ height: '77vh' }}>
                                        <DraggableCardCollectionResultModule search={search} ref={provided.innerRef}/>
                                    </div>
                                )
                            }
                        </Droppable>
                    </div>
                </DragDropContext>
            </div>
        </div>
    )
}