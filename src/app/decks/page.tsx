'use client'

import React from "react";
import { DashboardSearchBar } from "../../components/DashboardSearchBar";
import { Navbar } from "../../components/Navbar";
import { DeckCollectionResultModule } from "../../modules/DeckCollectionModule";

export default function Decks() {
    const [search, setSearch] = React.useState('')
    return (
        <div className="flex flex-col w-full h-screen">
            <div className="fixed w-full bg-gray-900">
                <Navbar/>
                <DashboardSearchBar setSearch={setSearch}/>
            </div>
            <div className="mt-40">
                <DeckCollectionResultModule search={search}/>
            </div>
        </div>
    )
}