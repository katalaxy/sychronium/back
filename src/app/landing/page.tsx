'use client'

import Link from "next/link";
import React from "react";
import { PricingCard } from "../../components/PricingCard";


export default function Landing() {
  const handleScroll = (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
    e.preventDefault();
    const href = e.currentTarget.href;
    const targetId = href.replace(/.*\#/, "");
    const elem = document.getElementById(targetId);
    elem?.scrollIntoView({
      behavior: "smooth",
    });
  };
  React.useEffect(() => {
    const fixedComponents = document.querySelectorAll('.fixedComponent');

    fixedComponents.forEach(function (fixedComponent: any) {
      const initialPosition = fixedComponent.offsetTop;

      window.addEventListener('scroll', function () {
        const scrollPosition = window.scrollY;

        if (scrollPosition >= initialPosition) {
          fixedComponent.classList.add('fixed', 'top-0', 'w-1/2', 'pr-8');
        } else {
          fixedComponent.classList.remove('fixed', 'top-0', 'w-1/2', 'pr-8');
        }
      });
    });
  }, [])
  return (
    <div className="App bg-gray-900">
      <header className="h-screen flex items-center justify-start relative flex-col">
        <img
          src="static/assets/images/1.png"
          alt="Background"
          className="object-cover w-full h-full absolute inset-0 z-0 blur-sm"
        />
        <div className="z-10 flex flex-col text-sky-300 h-full justify-center align-middle">
          <div className="border-8">
            <div className="m-9">
              <h1 className="text-9xl font-bold mb-8 leading-none justify-center align-middle">
                It's time to <br /> <div className="text-purple-500 font-extrabold">SYNCHRONIZE.</div>
              </h1>
              <div className="flex flex-row justify-center">
                <div className="m-2 mb-9">
                  <Link
                    href="/dashboard"
                    className="mx-auto py-3 px-6 bg-gray-800 hover:bg-gray-700 transition duration-300 rounded text-white text-2xl"
                  >
                    Connect
                  </Link>
                </div>
                <div className="m-2 mb-9">
                  <Link
                    href="#discoverSection"
                    onClick={handleScroll}
                    className="mx-auto py-3 px-6 bg-gray-700 hover:bg-gray-600 transition duration-300 rounded text-white text-2xl"
                  >
                    Discover ✨
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>

      <section id="discoverSection" className="h-screen bg-gray-900 mt-6 text-white p-8">
        <div className="flex flex-col bg-gray-900">
          <div className="flex flex-row bg-gray-900">
            <div className="flex flex-1 flex-col bg-gray-900 mt-6">
              <div className=" fixedComponent bg-gray-900 z-10">
                <h1 className="text-7xl font-bold bg-gray-900">
                  Syncronium.
                </h1>
                <h2 className="text-3xl bg-gray-900">
                  Your Yu-Gi-Oh! Card Collection Sync Solution
                </h2>



                <div className="flex flex-row align-middle h-full justify-center">
                  <div className="flex flex-col m-2 mb-9 w-full justify-center align-middle" style={{ height: '87vh' }}>
                    <Link
                      href="/api/auth/signin"
                      className="mx-auto py-5 px-20 bg-blue-700 hover:bg-blue-800 transition duration-300 rounded text-2xl"
                    >
                      Get started 🚀
                    </Link>
                  </div>
                </div>

              </div>
            </div>
            <div className="flex flex-1 flex-col bg-gray-900 z-10">
              <div className="h-screen justify-center align-middle bg-gray-900">
                <h2 className="text-4xl">
                  1. A web app
                </h2>
                <div className="flex flex-col h-full align-middle justify-center">
                  <div>
                    <div className="relative mx-auto border-gray-800 dark:border-gray-800 bg-gray-800 border-[8px] rounded-t-xl h-[172px] max-w-[301px] md:h-[294px] md:max-w-[512px]">
                      <div className="rounded-lg overflow-hidden h-[156px] md:h-[278px] bg-white dark:bg-gray-800">
                        <img src="https://flowbite.s3.amazonaws.com/docs/device-mockups/laptop-screen.png" className="dark:hidden h-[156px] md:h-[278px] w-full rounded-xl" alt="" />
                        <img src="https://flowbite.s3.amazonaws.com/docs/device-mockups/laptop-screen-dark.png" className="hidden dark:block h-[156px] md:h-[278px] w-full rounded-lg" alt="" />
                      </div>
                    </div>
                    <div className="relative mx-auto bg-gray-900 dark:bg-gray-700 rounded-b-xl rounded-t-sm h-[17px] max-w-[351px] md:h-[21px] md:max-w-[597px]">
                      <div className="absolute left-1/2 top-0 -translate-x-1/2 rounded-b-xl w-[56px] h-[5px] md:w-[96px] md:h-[8px] bg-gray-800"></div>
                    </div>
                  </div>
                  <div className="flex flex-col w-full align-middle mt-40">
                    <Link
                      href="/api/auth/signin"
                      className="mx-auto py-5 px-20 bg-gray-800 hover:bg-gray-700 transition duration-300 rounded text-2xl"
                    >
                      Get a free account 🔑
                    </Link>
                  </div>
                </div>
              </div>
              <div className="h-screen justify-center align-middle bg-gray-900">
                <h2 className="text-4xl">
                  2. A mobile app
                </h2>
                <div className="flex flex-col align-middle justify-center h-full">
                  <div>
                    <div className="relative mx-auto border-gray-800 dark:border-gray-800 bg-gray-800 border-[14px] rounded-[2.5rem] h-[600px] w-[300px] shadow-xl">
                      <div className="w-[148px] h-[18px] bg-gray-800 top-0 rounded-b-[1rem] left-1/2 -translate-x-1/2 absolute"></div>
                      <div className="h-[46px] w-[3px] bg-gray-800 absolute -start-[17px] top-[124px] rounded-s-lg"></div>
                      <div className="h-[46px] w-[3px] bg-gray-800 absolute -start-[17px] top-[178px] rounded-s-lg"></div>
                      <div className="h-[64px] w-[3px] bg-gray-800 absolute -end-[17px] top-[142px] rounded-e-lg"></div>
                      <div className="rounded-[2rem] overflow-hidden w-[272px] h-[572px] bg-white dark:bg-gray-800">
                        <img src="https://flowbite.s3.amazonaws.com/blocks/marketing-ui/hero/mockup-2-light.png" className="dark:hidden w-[272px] h-[572px]" alt="" />
                        <img src="https://flowbite.s3.amazonaws.com/blocks/marketing-ui/hero/mockup-2-dark.png" className="hidden dark:block w-[272px] h-[572px]" alt="" />
                      </div>
                    </div>
                  </div>
                  <div className="flex flex-row my-11 justify-center">
                    <div className="flex h-full justify-center align-middle">
                      <div className="flex row flex-1 justify-end mr-4">
                        <a className="bg-gray-700 inline-flex py-3 px-5 rounded-lg items-center hover:bg-gray-600 focus:outline-none">
                          <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" className="w-6 h-6" viewBox="0 0 512 512">
                            <path d="M99.617 8.057a50.191 50.191 0 00-38.815-6.713l230.932 230.933 74.846-74.846L99.617 8.057zM32.139 20.116c-6.441 8.563-10.148 19.077-10.148 30.199v411.358c0 11.123 3.708 21.636 10.148 30.199l235.877-235.877L32.139 20.116zM464.261 212.087l-67.266-37.637-81.544 81.544 81.548 81.548 67.273-37.64c16.117-9.03 25.738-25.442 25.738-43.908s-9.621-34.877-25.749-43.907zM291.733 279.711L60.815 510.629c3.786.891 7.639 1.371 11.492 1.371a50.275 50.275 0 0027.31-8.07l266.965-149.372-74.849-74.847z"></path>
                          </svg>
                          <span className="ml-4 flex items-start flex-col leading-none">
                            <span className="text-xs text-gray-200 mb-1">GET IT ON</span>
                            <span className="title-font font-medium">Google Play</span>
                          </span>
                        </a>
                      </div>
                      <div className="flex row flex-1 justify-start ml-4">
                        <a className="bg-gray-700 inline-flex py-3 px-5 rounded-lg items-center hover:bg-gray-600 focus:outline-none">
                          <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" className="w-6 h-6" viewBox="0 0 305 305">
                            <path d="M40.74 112.12c-25.79 44.74-9.4 112.65 19.12 153.82C74.09 286.52 88.5 305 108.24 305c.37 0 .74 0 1.13-.02 9.27-.37 15.97-3.23 22.45-5.99 7.27-3.1 14.8-6.3 26.6-6.3 11.22 0 18.39 3.1 25.31 6.1 6.83 2.95 13.87 6 24.26 5.81 22.23-.41 35.88-20.35 47.92-37.94a168.18 168.18 0 0021-43l.09-.28a2.5 2.5 0 00-1.33-3.06l-.18-.08c-3.92-1.6-38.26-16.84-38.62-58.36-.34-33.74 25.76-51.6 31-54.84l.24-.15a2.5 2.5 0 00.7-3.51c-18-26.37-45.62-30.34-56.73-30.82a50.04 50.04 0 00-4.95-.24c-13.06 0-25.56 4.93-35.61 8.9-6.94 2.73-12.93 5.09-17.06 5.09-4.64 0-10.67-2.4-17.65-5.16-9.33-3.7-19.9-7.9-31.1-7.9l-.79.01c-26.03.38-50.62 15.27-64.18 38.86z"></path>
                            <path d="M212.1 0c-15.76.64-34.67 10.35-45.97 23.58-9.6 11.13-19 29.68-16.52 48.38a2.5 2.5 0 002.29 2.17c1.06.08 2.15.12 3.23.12 15.41 0 32.04-8.52 43.4-22.25 11.94-14.5 17.99-33.1 16.16-49.77A2.52 2.52 0 00212.1 0z"></path>
                          </svg>
                          <span className="ml-4 flex items-start flex-col leading-none">
                            <span className="text-xs text-gray-200 mb-1">Download on the</span>
                            <span className="title-font font-medium">App Store</span>
                          </span>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="h-screen justify-center align-middle bg-gray-900">
                <h2 className="text-4xl">
                  3. Always free
                </h2>
                <div className="flex flex-col h-full justify-center align-middle">
                  <div className="flex flex-row justify-center">

                    <PricingCard avaliable={["Up to 5 decks", "Card registering"]} notAvaliable={["Priority support", "Deck builder"]} currency="$" period="month" planName="Free Plan" price={0} />
                    <PricingCard avaliable={["Unlimited decks", "Card registering", "Priority support", "Deck builder"]} notAvaliable={["Candles dinner with devs"]} currency="$" period="month" planName="Sponsor Plan" price={2} />
                  </div>
                </div>
              </div>
              <div className="h-screen flex flex-col align-middle bg-gray-900">
                <h2 className="text-4xl">
                  4. Open source
                </h2>
                <div className="flex flex-col f1 h-full">
                  {/* section content */}
                </div>
                <footer className="bg-white dark:bg-gray-900">
                  <div className="mx-auto w-full max-w-screen-xl p-4 py-6 lg:py-8">
                    <div className="md:flex md:justify-between">
                      <div className="mb-6 md:mb-0">
                        <a href="#" className="flex items-center">
                          <span className="self-center text-2xl font-semibold whitespace-nowrap dark:text-white">Synchronium</span>
                        </a>
                      </div>
                      <div className="grid grid-cols-2 gap-8 sm:gap-6 sm:grid-cols-3">
                        <div>
                          <h2 className="mb-6 text-sm font-semibold text-gray-900 uppercase dark:text-white">Resources</h2>
                          <ul className="text-gray-500 dark:text-gray-400 font-medium">
                            <li className="mb-4">
                              <a href="https://flowbite.com/" className="hover:underline">Flowbite</a>
                            </li>
                            <li>
                              <a href="https://tailwindcss.com/" className="hover:underline">Tailwind CSS</a>
                            </li>
                          </ul>
                        </div>
                        <div>
                          <h2 className="mb-6 text-sm font-semibold text-gray-900 uppercase dark:text-white">Follow us</h2>
                          <ul className="text-gray-500 dark:text-gray-400 font-medium">
                            <li className="mb-4">
                              <a href="https://github.com/themesberg/flowbite" className="hover:underline ">Github</a>
                            </li>
                            <li>
                              <a href="https://discord.gg/4eeurUVvTy" className="hover:underline">Discord</a>
                            </li>
                          </ul>
                        </div>
                        <div>
                          <h2 className="mb-6 text-sm font-semibold text-gray-900 uppercase dark:text-white">Legal</h2>
                          <ul className="text-gray-500 dark:text-gray-400 font-medium">
                            <li className="mb-4">
                              <a href="#" className="hover:underline">Privacy Policy</a>
                            </li>
                            <li>
                              <a href="#" className="hover:underline">Terms &amp; Conditions</a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <hr className="my-6 border-gray-200 sm:mx-auto dark:border-gray-700 lg:my-8" />
                    <div className="sm:flex sm:items-center sm:justify-between">
                      <span className="text-sm text-gray-500 sm:text-center dark:text-gray-400">© 2023 <a href="#" className="hover:underline">Synchronium</a>. All Rights Reserved.
                      </span>
                      <div className="flex mt-4 sm:justify-center sm:mt-0">
                        <a href="#" className="text-gray-500 hover:text-gray-900 dark:hover:text-white">
                          <svg className="w-4 h-4" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 8 19">
                            <path fillRule="evenodd" d="M6.135 3H8V0H6.135a4.147 4.147 0 0 0-4.142 4.142V6H0v3h2v9.938h3V9h2.021l.592-3H5V3.591A.6.6 0 0 1 5.592 3h.543Z" clipRule="evenodd" />
                          </svg>
                          <span className="sr-only">Facebook page</span>
                        </a>
                        <a href="#" className="text-gray-500 hover:text-gray-900 dark:hover:text-white ms-5">
                          <svg className="w-4 h-4" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 21 16">
                            <path d="M16.942 1.556a16.3 16.3 0 0 0-4.126-1.3 12.04 12.04 0 0 0-.529 1.1 15.175 15.175 0 0 0-4.573 0 11.585 11.585 0 0 0-.535-1.1 16.274 16.274 0 0 0-4.129 1.3A17.392 17.392 0 0 0 .182 13.218a15.785 15.785 0 0 0 4.963 2.521c.41-.564.773-1.16 1.084-1.785a10.63 10.63 0 0 1-1.706-.83c.143-.106.283-.217.418-.33a11.664 11.664 0 0 0 10.118 0c.137.113.277.224.418.33-.544.328-1.116.606-1.71.832a12.52 12.52 0 0 0 1.084 1.785 16.46 16.46 0 0 0 5.064-2.595 17.286 17.286 0 0 0-2.973-11.59ZM6.678 10.813a1.941 1.941 0 0 1-1.8-2.045 1.93 1.93 0 0 1 1.8-2.047 1.919 1.919 0 0 1 1.8 2.047 1.93 1.93 0 0 1-1.8 2.045Zm6.644 0a1.94 1.94 0 0 1-1.8-2.045 1.93 1.93 0 0 1 1.8-2.047 1.918 1.918 0 0 1 1.8 2.047 1.93 1.93 0 0 1-1.8 2.045Z" />
                          </svg>
                          <span className="sr-only">Discord community</span>
                        </a>
                        <a href="#" className="text-gray-500 hover:text-gray-900 dark:hover:text-white ms-5">
                          <svg className="w-4 h-4" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 20 17">
                            <path fillRule="evenodd" d="M20 1.892a8.178 8.178 0 0 1-2.355.635 4.074 4.074 0 0 0 1.8-2.235 8.344 8.344 0 0 1-2.605.98A4.13 4.13 0 0 0 13.85 0a4.068 4.068 0 0 0-4.1 4.038 4 4 0 0 0 .105.919A11.705 11.705 0 0 1 1.4.734a4.006 4.006 0 0 0 1.268 5.392 4.165 4.165 0 0 1-1.859-.5v.05A4.057 4.057 0 0 0 4.1 9.635a4.19 4.19 0 0 1-1.856.07 4.108 4.108 0 0 0 3.831 2.807A8.36 8.36 0 0 1 0 14.184 11.732 11.732 0 0 0 6.291 16 11.502 11.502 0 0 0 17.964 4.5c0-.177 0-.35-.012-.523A8.143 8.143 0 0 0 20 1.892Z" clipRule="evenodd" />
                          </svg>
                          <span className="sr-only">Twitter page</span>
                        </a>
                        <a href="#" className="text-gray-500 hover:text-gray-900 dark:hover:text-white ms-5">
                          <svg className="w-4 h-4" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 20 20">
                            <path fillRule="evenodd" d="M10 .333A9.911 9.911 0 0 0 6.866 19.65c.5.092.678-.215.678-.477 0-.237-.01-1.017-.014-1.845-2.757.6-3.338-1.169-3.338-1.169a2.627 2.627 0 0 0-1.1-1.451c-.9-.615.07-.6.07-.6a2.084 2.084 0 0 1 1.518 1.021 2.11 2.11 0 0 0 2.884.823c.044-.503.268-.973.63-1.325-2.2-.25-4.516-1.1-4.516-4.9A3.832 3.832 0 0 1 4.7 7.068a3.56 3.56 0 0 1 .095-2.623s.832-.266 2.726 1.016a9.409 9.409 0 0 1 4.962 0c1.89-1.282 2.717-1.016 2.717-1.016.366.83.402 1.768.1 2.623a3.827 3.827 0 0 1 1.02 2.659c0 3.807-2.319 4.644-4.525 4.889a2.366 2.366 0 0 1 .673 1.834c0 1.326-.012 2.394-.012 2.72 0 .263.18.572.681.475A9.911 9.911 0 0 0 10 .333Z" clipRule="evenodd" />
                          </svg>
                          <span className="sr-only">GitHub account</span>
                        </a>
                        <a href="#" className="text-gray-500 hover:text-gray-900 dark:hover:text-white ms-5">
                          <svg className="w-4 h-4" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 20 20">
                            <path fillRule="evenodd" d="M10 0a10 10 0 1 0 10 10A10.009 10.009 0 0 0 10 0Zm6.613 4.614a8.523 8.523 0 0 1 1.93 5.32 20.094 20.094 0 0 0-5.949-.274c-.059-.149-.122-.292-.184-.441a23.879 23.879 0 0 0-.566-1.239 11.41 11.41 0 0 0 4.769-3.366ZM8 1.707a8.821 8.821 0 0 1 2-.238 8.5 8.5 0 0 1 5.664 2.152 9.608 9.608 0 0 1-4.476 3.087A45.758 45.758 0 0 0 8 1.707ZM1.642 8.262a8.57 8.57 0 0 1 4.73-5.981A53.998 53.998 0 0 1 9.54 7.222a32.078 32.078 0 0 1-7.9 1.04h.002Zm2.01 7.46a8.51 8.51 0 0 1-2.2-5.707v-.262a31.64 31.64 0 0 0 8.777-1.219c.243.477.477.964.692 1.449-.114.032-.227.067-.336.1a13.569 13.569 0 0 0-6.942 5.636l.009.003ZM10 18.556a8.508 8.508 0 0 1-5.243-1.8 11.717 11.717 0 0 1 6.7-5.332.509.509 0 0 1 .055-.02 35.65 35.65 0 0 1 1.819 6.476 8.476 8.476 0 0 1-3.331.676Zm4.772-1.462A37.232 37.232 0 0 0 13.113 11a12.513 12.513 0 0 1 5.321.364 8.56 8.56 0 0 1-3.66 5.73h-.002Z" clipRule="evenodd" />
                          </svg>
                          <span className="sr-only">Dribbble account</span>
                        </a>
                      </div>
                    </div>
                  </div>
                </footer>
              </div>
            </div>
          </div>
        </div>
      </section >
    </div >

  )
}
