'use client'

import { useState } from "react"
import { DashboardSearchBar } from "../../components/DashboardSearchBar"
import { Navbar } from "../../components/Navbar"
import { CardCollectionResultModule } from "../../modules/CardsCollectionResultModule"

export default function Dashboard() {
    const [search, setSearch] = useState('')
    const [cardCollectionTrigger, setCardCollectionTrigger] = useState(0)
    return (
        <div className="flex flex-col w-full">
            <div className="fixed w-full bg-gray-900 z-30">
                <Navbar setReloadTrigger={setCardCollectionTrigger}/>
                <DashboardSearchBar setSearch={setSearch}/>
            </div>
            <div className="mt-40">
                <CardCollectionResultModule search={search} trigger={cardCollectionTrigger} setTrigger={setCardCollectionTrigger}/>
            </div>
        </div>
    )
}