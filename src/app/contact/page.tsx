'use client'

import React from 'react';
import { Navbar } from '../../components/Navbar';
import Swal from 'sweetalert2';
import { sendMessage } from '../../api/sendMessage';

export default function Contact() {
    const [message, setMessage] = React.useState<string>('');
    const [subject, setSubject] = React.useState<string>('');
    const [email, setEmail] = React.useState<string>('');
    return (
        <div className="flex flex-col w-full h-screen">
            <div className="fixed w-full bg-gray-900">
                <Navbar/>
            </div>
            <div className="flex flex-row mt-20 h-full w-full justify-center">
                <div className="flex flex-col h-full justify-center align-middle w-1/2">
                    <div className="">
                        <form className="mb-6" action="javascript:void(0);" onSubmit={() => {
                            sendMessage(email, subject, message).then(() =>{
                                Swal.fire({icon: 'success', title: 'The Synchronium team thanks you for your message !', text: 'You will recive a recap in your mail box, we will answer soon.'}).then(() => window.location.reload())
                            }).catch(() => Swal.fire({icon: 'error', title: 'An error occured, please try again later !'}))
                        }}>
                            <div className="mb-6">
                                <label htmlFor="email" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Your email</label>
                                <input onChange={(e) => setEmail(e.target.value)} type="email" id="email" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="seto.kaiba@kaiba.corp" required />
                            </div>
                            <div className="mb-6">
                                <label htmlFor="subject" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Subject</label>
                                <input onChange={(e) => setSubject(e.target.value)}  type="text" id="subject" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Let us know how we can help you" required />
                            </div>
                            <div className="mb-6">
                                <label htmlFor="message" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Your message</label>
                                <textarea onChange={(e) => setMessage(e.target.value)} id="message" rows={12} className="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Your message..." required></textarea>
                            </div>
                            <button type="submit" className="text-white bg-blue-700 hover:bg-blue-800 w-full focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800 block">Send message</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}