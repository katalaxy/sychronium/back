generator client {
  provider = "prisma-client-js"
}

datasource db {
  provider = "postgres"
  url      = env("POSTGRES_URL")
}

// Card model
model Card {
  card_index     Int      @id @default(autoincrement())
  id             Int
  name           String
  name_en        String?
  type           String
  frameType      String
  desc           String
  race           String
  atk            Int?
  def            Int?
  level          Int?
  attribute      String?
  linkval        Int?
  linkmarkers    String[]
  archetype      String?
  pend_desc      String?
  monster_desc   String?
  scale          Int?
  ygoprodeck_url String

  // Attributes outside yogopro data set for querying / caching
  is_images_cached Boolean
  language         String

  // Card Set relation
  card_sets CardSet[]

  // Card Images relation
  card_images CardImage[]

  // Card Prices relation
  card_prices CardPrice[]

  // Card Banlist Info relation
  banlist_info banlistInfo?

  // User Card relation
  UserCard UserCard[]
}

// CardSet model
model CardSet {
  id              Int    @id @default(autoincrement())
  set_name        String
  set_code        String
  set_rarity      String
  set_rarity_code String
  set_price       String
  card            Card   @relation(fields: [cardId], references: [card_index])
  cardId          Int
}

// CardImage model
model CardImage {
  image_index       Int    @id @default(autoincrement())
  id                Int
  image_url         String
  image_url_small   String
  image_url_cropped String
  card              Card   @relation(fields: [cardId], references: [card_index])
  cardId            Int
}

// CardPrice model
model CardPrice {
  id                 Int    @id @default(autoincrement())
  cardmarket_price   String
  tcgplayer_price    String
  ebay_price         String
  amazon_price       String
  coolstuffinc_price String
  card               Card   @relation(fields: [cardId], references: [card_index])
  cardId             Int
}

//model banlist
model banlistInfo {
  id       Int     @id @default(autoincrement())
  ban_tcg  String?
  ban_ocg  String?
  ban_goat String?
  card     Card    @relation(fields: [cardId], references: [card_index])
  cardId   Int     @unique
}

model UserCard {
  id           Int         @id @default(autoincrement())
  user         User        @relation(fields: [userId], references: [id])
  userId       String
  card         Card?       @relation(fields: [card_index], references: [card_index])
  card_index   Int?
  set_code     String
  Collection   Collection? @relation(fields: [collectionId], references: [id])
  collectionId Int?
  Deck         Deck[]
}

model Collection {
  id     Int        @id @default(autoincrement())
  cards  UserCard[]
  user   User       @relation(fields: [userId], references: [id])
  userId String     @unique
}

model Deck {
  id     Int        @id @default(autoincrement())
  cards  UserCard[]
  User   User?      @relation(fields: [userId], references: [id])
  userId String
  name   String
}

model User {
  id            String          @id @default(cuid())
  name          String?
  email         String          @unique
  emailVerified DateTime?
  image         String?
  accounts      Account[]
  sessions      Session[]
  Authenticator Authenticator[]
  collection    Collection?
  decks         Deck[]
  userCards     UserCard[]
}

model Account {
  userId            String
  type              String
  provider          String
  providerAccountId String
  refresh_token     String?
  access_token      String?
  expires_at        Int?
  token_type        String?
  scope             String?
  id_token          String?
  session_state     String?

  user User @relation(fields: [userId], references: [id], onDelete: Cascade)

  @@id([provider, providerAccountId])
}

model Session {
  sessionToken String   @unique
  userId       String
  expires      DateTime
  user         User     @relation(fields: [userId], references: [id], onDelete: Cascade)
}

model VerificationToken {
  identifier String
  token      String   @unique
  expires    DateTime

  @@id([identifier, token])
}

model Authenticator {
  id                   String  @id @default(cuid())
  credentialID         String  @unique
  userId               String
  providerAccountId    String
  credentialPublicKey  String
  counter              Int
  credentialDeviceType String
  credentialBackedUp   Boolean
  transports           String?

  user User @relation(fields: [userId], references: [id], onDelete: Cascade)
}
