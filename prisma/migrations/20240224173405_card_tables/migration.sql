-- CreateTable
CREATE TABLE "Card" (
    "card_index" SERIAL NOT NULL,
    "id" INTEGER NOT NULL,
    "name" TEXT NOT NULL,
    "name_en" TEXT,
    "type" TEXT NOT NULL,
    "frameType" TEXT NOT NULL,
    "desc" TEXT NOT NULL,
    "race" TEXT NOT NULL,
    "atk" INTEGER,
    "def" INTEGER,
    "level" INTEGER,
    "attribute" TEXT,
    "linkval" INTEGER,
    "linkmarkers" TEXT[],
    "archetype" TEXT,
    "pend_desc" TEXT,
    "monster_desc" TEXT,
    "scale" INTEGER,
    "ygoprodeck_url" TEXT NOT NULL,
    "is_images_cached" BOOLEAN NOT NULL,
    "language" TEXT NOT NULL,

    CONSTRAINT "Card_pkey" PRIMARY KEY ("card_index")
);

-- CreateTable
CREATE TABLE "CardSet" (
    "id" SERIAL NOT NULL,
    "set_name" TEXT NOT NULL,
    "set_code" TEXT NOT NULL,
    "set_rarity" TEXT NOT NULL,
    "set_rarity_code" TEXT NOT NULL,
    "set_price" TEXT NOT NULL,
    "cardId" INTEGER NOT NULL,

    CONSTRAINT "CardSet_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "CardImage" (
    "image_index" SERIAL NOT NULL,
    "id" INTEGER NOT NULL,
    "image_url" TEXT NOT NULL,
    "image_url_small" TEXT NOT NULL,
    "image_url_cropped" TEXT NOT NULL,
    "cardId" INTEGER NOT NULL,

    CONSTRAINT "CardImage_pkey" PRIMARY KEY ("image_index")
);

-- CreateTable
CREATE TABLE "CardPrice" (
    "id" SERIAL NOT NULL,
    "cardmarket_price" TEXT NOT NULL,
    "tcgplayer_price" TEXT NOT NULL,
    "ebay_price" TEXT NOT NULL,
    "amazon_price" TEXT NOT NULL,
    "coolstuffinc_price" TEXT NOT NULL,
    "cardId" INTEGER NOT NULL,

    CONSTRAINT "CardPrice_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "banlistInfo" (
    "id" SERIAL NOT NULL,
    "ban_tcg" TEXT,
    "ban_ocg" TEXT,
    "ban_goat" TEXT,
    "cardId" INTEGER NOT NULL,

    CONSTRAINT "banlistInfo_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "banlistInfo_cardId_key" ON "banlistInfo"("cardId");

-- AddForeignKey
ALTER TABLE "CardSet" ADD CONSTRAINT "CardSet_cardId_fkey" FOREIGN KEY ("cardId") REFERENCES "Card"("card_index") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CardImage" ADD CONSTRAINT "CardImage_cardId_fkey" FOREIGN KEY ("cardId") REFERENCES "Card"("card_index") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CardPrice" ADD CONSTRAINT "CardPrice_cardId_fkey" FOREIGN KEY ("cardId") REFERENCES "Card"("card_index") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "banlistInfo" ADD CONSTRAINT "banlistInfo_cardId_fkey" FOREIGN KEY ("cardId") REFERENCES "Card"("card_index") ON DELETE RESTRICT ON UPDATE CASCADE;
