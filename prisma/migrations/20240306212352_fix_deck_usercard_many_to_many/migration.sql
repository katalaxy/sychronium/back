/*
  Warnings:

  - You are about to drop the column `deckId` on the `UserCard` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE "UserCard" DROP CONSTRAINT "UserCard_deckId_fkey";

-- AlterTable
ALTER TABLE "UserCard" DROP COLUMN "deckId";

-- CreateTable
CREATE TABLE "_DeckToUserCard" (
    "A" INTEGER NOT NULL,
    "B" INTEGER NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "_DeckToUserCard_AB_unique" ON "_DeckToUserCard"("A", "B");

-- CreateIndex
CREATE INDEX "_DeckToUserCard_B_index" ON "_DeckToUserCard"("B");

-- AddForeignKey
ALTER TABLE "_DeckToUserCard" ADD CONSTRAINT "_DeckToUserCard_A_fkey" FOREIGN KEY ("A") REFERENCES "Deck"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_DeckToUserCard" ADD CONSTRAINT "_DeckToUserCard_B_fkey" FOREIGN KEY ("B") REFERENCES "UserCard"("id") ON DELETE CASCADE ON UPDATE CASCADE;
