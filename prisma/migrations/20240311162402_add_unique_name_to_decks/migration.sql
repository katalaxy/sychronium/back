/*
  Warnings:

  - A unique constraint covering the columns `[name]` on the table `Deck` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `name` to the `Deck` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Deck" ADD COLUMN     "name" TEXT NOT NULL;

-- CreateIndex
CREATE UNIQUE INDEX "Deck_name_key" ON "Deck"("name");
