import { PrismaClient, Prisma } from '@prisma/client'
import fr from '../fr.json'
import en from '../en.json'

const prisma = new PrismaClient()

const cardData = [
  ...(fr as any).data.map((card: any) => ({ ...card, language: 'FR', is_images_cached: false})),
  ...(en as any).data.map((card: any) => ({ ...card, language: 'EN', is_images_cached: false}))
] as unknown as Prisma.CardCreateInput[]

async function main() {
  console.log(`Start seeding ${cardData.length} elements`)
  for (const u of cardData) {
    const card = await prisma.card.create({
      data: {
        ...u,
        card_sets: {
          create: u.card_sets as any
        },
        card_images: {
          create: u.card_images as any
        },
        card_prices: {
          create: u.card_prices as any
        },
        banlist_info: u.banlist_info ? {create: u.banlist_info} : undefined,
      },
    })
    console.log(`Created card with id: ${card.id}`)
  }
  console.log(`Aller hop ! ${cardData.length} cartes en plein dans la pense !`)
}

main()
  .then(async () => {
    await prisma.$disconnect()
  })
  .catch(async (e) => {
    console.error(e)
    await prisma.$disconnect()
    process.exit(1)
  })