import type { GlobalProvider } from '@ladle/react'
import '../src/styles/global.css'
import 'sweetalert2/src/sweetalert2.scss'
import React from 'react'

export const Provider: GlobalProvider = ({ children, globalState }) => (
  <>{children}</>
)