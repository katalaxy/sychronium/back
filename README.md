
# Synchronium

Synchronium is a project that combines an API and a website, built with Tailwind CSS, Prisma ORM, and Next.js.

## Table of Contents
- [Technologies](#technologies)
- [Installation](#installation)
- [Usage](#usage)
- [Commands](#commands)
- [Tools Setup (Optional)](#tools-setup-optional)
- [Contributing](#contributing)
- [License](#license)

## Technologies
- [Tailwind CSS](https://tailwindcss.com/docs)
- [Prisma ORM](https://www.prisma.io/docs)
- [Next.js](https://nextjs.org/docs)

## Installation

To set up the project, follow these steps:

1. Install 'just' by following the instructions in the [just GitHub repository](https://github.com/casey/just?tab=readme-ov-file#packages).
2. Install Docker by following the instructions in the [Docker documentation](https://docs.docker.com/engine/install/).
3. Optionally, you can set up tools for the project using either [asdf](https://asdf-vm.com/guide/getting-started.html) or [mise](https://mise.jdx.dev/getting-started.html) with the command `mise install` or `asdf install` at the root of the project.
4. Install project dependencies: 
   ```bash
   yarn
   ```
5. Generate Prisma CLI:
   ```bash
   just generate
   ```
6. Run migrations:
   ```bash
   just migrate
   ```

## Usage

To run the project, use the following command:
```bash
just run
```

## Commands

For command-line operations, you can use the following commands:

- Install dependencies: `yarn`
- Generate Prisma CLI: `just generate`
- Run migrations (run seeder if the base is empty, might be long): `just migrate`
- Run the project: `just run`

## Tools Setup (Optional)

You can set up tools for the project using either [asdf](https://asdf-vm.com/guide/getting-started.html) or [mise](https://mise.jdx.dev/getting-started.html) with the command `mise install` or `asdf install` at the root of the project.

## Contributing

If you would like to contribute to the project, please follow the guidelines in [CONTRIBUTING.md](CONTRIBUTING.md).

## License

This project is licensed under the [MIT License](LICENSE).